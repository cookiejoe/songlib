package model;
/*
 * SongLib Project
 * Team Byte Me;
 * Members: Joseph Buchoff, Julian Dauemer
 * 
 * Professor Seshrandi Venugopal
 * 
 * Fuel - Exercise and healthy eating
 * Hours spent - Too many
 * Walls punched - Let's not talk about it
 * 
 * Grade desired - A, obviously
 * Grade earned...? Undefined
 */


public class SameSongException extends Exception
{
	//Parameterless Constructor
	public SameSongException() {}

	//Constructor that accepts a message
	public SameSongException(String message)
	{
		super(message);
	}
	
}
