package controller;

import javafx.collections.ObservableList;

import java.util.Optional;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.SameSongException;
import model.Song;

import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class SongLibController {
	
	   //Define an enum to store task for the submit button
	   public enum Task
	   {
		   Add, Delete, Edit
	   }
	   
	   // Initialize a Task to hold add, delete or edit
	   private static Task task;
	
	   // Initialize all the FXML stuff
	   @FXML         
	   ListView<String> songListView = new ListView<String>();
	   @FXML
	   TextField nameField = new TextField();
	   @FXML
	   TextField artistField = new TextField();
	   @FXML
	   TextField albumField = new TextField();
	   @FXML
	   TextField yearField = new TextField();
	   @FXML
	   Button addButton = new Button("Add");
	   @FXML
	   Button deleteButton = new Button("Delete");
	   @FXML
	   Button editButton = new Button("Edit");
	   @FXML
	   Button submitButton = new Button("Submit");
	   
	   // Create a class variable for mainStage so we can display the popup from the Submit Button's
	   // no-arg listener method
	   Stage mainStage = null;
	   
	   // Initialize an ObservableList for the ListView
	   private ObservableList<String> obsList;
	   
	   public void start(Stage mainStage)
	   {
		   // Store mainStage in this instance so we can use it for the submit button's click listener
		   this.mainStage = mainStage;
		   
		   // Update the list of songs being shown
		   	updateSongs(0);
		   	
		   	// If there is at least one object in the list, select the first one
		   	if (obsList.size() > 0) songListView.getSelectionModel().select(0);
		   	
		   	// Set listener for the ListView which changes the info based on which song is selected
		   	songListView
		   		.getSelectionModel()
		        .selectedIndexProperty()
		        .addListener((obsv, oldv, newv) ->
		        		updateSongs(songListView.getSelectionModel().getSelectedIndex()));
		   
		    // Set col. count for all the text fields
			nameField.setPrefColumnCount(30);
			artistField.setPrefColumnCount(20);
			albumField.setPrefColumnCount(30);
			yearField.setPrefColumnCount(4);
	   	}
	   
		
		// Update the songs shown to reflect the actual songs
		private void updateSongs(int selectedIndex)
		{
			obsList = FXCollections.observableArrayList(Song.getDisplayableList(selectedIndex));
			songListView.setItems(obsList);
			
			if (obsList.size() > 0)
			{
				songListView.getSelectionModel().select(selectedIndex);
			}
		}
		
		// Methods to consolidate complicated code and improve readability
		private int selectedIndex() {return songListView.getSelectionModel().getSelectedIndex();}
		private Song selectedSong() {return Song.getSongs().get(selectedIndex());}

		/*
		 *  Define Alert for when user tries to create a new 
		 *  entry that has the same song name and artist name
		 *  as an existing entry
		 */
		public void duplicateEntryAlert(Stage mainStage) {                
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage);
			alert.setTitle("Duplicate Entry");
			alert.setHeaderText("Error: Song with this name and artist already exist");
			alert.showAndWait();
		}
		
		/*
		 *  Define an Alert that allows a user to cancel an add/edit/delete action.
		 *  It returns true if the user wishes to continue the action, and false if the user wishes to cancel
		 */
		public boolean confirmationAlert(Stage mainStage) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.initOwner(mainStage);
			alert.setTitle("Confirmation Action");
			alert.setHeaderText("Are you sure?");
			
			Optional<ButtonType> result = alert.showAndWait();
			return (result.get() == ButtonType.OK) ? true : false;
		}
		
		
		@FXML
		private void addButtonClicked()
		{
			// Now that we're adding a song, update all the prompts
			nameField.setPromptText("Song Name");
			artistField.setPromptText("Artist");
			albumField.setPromptText("Album");
			yearField.setPromptText("Year");
			
			submitButton.setText("Add Song");

			// TODO Show text fields
			
			// Set the task to Add so the submitButton does the right thing
			task = Task.Add;				
		}
		
		@FXML
		public void deleteButtonClicked()
		{
			// Now that we're deleting a song, update the submit prompt
			submitButton.setText("Delete Song");
			
			// Set the task to Delete so the submitButton does the right thing
			task = Task.Delete;
		}
		
		@FXML
		public void editButtonClicked()
		{
			// Now that we're editing a song, update all the prompts
			nameField.setPromptText(selectedSong().getName());
			artistField.setPromptText(selectedSong().getArtist());
			albumField.setPromptText(selectedSong().getAlbum());
			yearField.setPromptText(selectedSong().getYear());
			
			submitButton.setText("Edit Song");
			
			// Set the task to Edit so the submitButton does the right thing
			task = Task.Edit;
		}
		
		@FXML
		public void submitButtonClicked()
		{
			// Variable to hold the index
			int index;
			index = (selectedIndex()<0)?0:selectedIndex();
			
			switch (task)
			{
				case Add:
					// Confirm with User first
					if (!confirmationAlert(mainStage))
					{
						break;
					}	
						
					// Add the song expressed in the text fields to the whole array
					try {
						
						// The constructor adds the song to the list, we don't need to keep the Song object, we just need to create it
						Song throwaway = new Song(nameField.getText(), artistField.getText(), albumField.getText(), yearField.getText());
						
						// Set the index to the new song
						index = Song.getSongs().indexOf(throwaway);
					} catch (SameSongException e) {
						duplicateEntryAlert(mainStage);
					}
					
					break;
					
				case Delete:
					// Confirm with User first
					if (!confirmationAlert(mainStage))
					{
						break;
					}	
					
					// Deletes the currently-selected song
					Song.deleteSong(selectedSong());

					// If we deleted the last song, let's highlight the one before it
					if (index > (Song.getSongs().size() - 1))
						index --;
					
					break;
					
				case Edit:
					// Confirm with User first
					if (!confirmationAlert(mainStage))
					{
						break;
					}	

					// Edits the song with the new info from the field
					try
					{
						Song.editSong(selectedSong(), nameField.getText(), artistField.getText(), albumField.getText(), yearField.getText());
					} catch (SameSongException sse) {
						duplicateEntryAlert(mainStage);
					}
					
					break;
			}
			
			// Update the songs
			updateSongs(index);

			// TODO hide the text fields
		}
			
}
