package application;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.*;

import model.SameSongException;
import model.Song;
import model.FileIO;

import controller.SongLibController;

import java.io.IOException;

import javax.json.*;

//import javafx.event.ActionEvent;

public class SongLib extends Application
{
	
	private static final String saveFile = "file.txt";
	
	public static void main(String[] args)
	{
		// Set the FileIO file field, and read the file into data
		FileIO.setFilename(saveFile);
		FileIO.readFile();
		
		// Load the songs from the data
		loadSongs();
		
		Application.launch();
		
		// Save the songs into memory for next time
		FileIO.writeFile(Song.getSongs());
	}
	
	/*
	 * Loads all the songs from the text file into Song.songs
	 */
	private static void loadSongs() {
		// Get the next JSON Object while there are still more
		String[] songInfo;
		while ((songInfo = FileIO.nextSong()) != null)
		{
			try
			{
				Song.addSong(new Song(songInfo[0]/*Song Name*/, songInfo[1]/*Artist*/,
					songInfo[2]/*Album*/, songInfo[3]/*Year*/));
			} catch (SameSongException e) {
				
			}
		}
	}
	
	@ Override
	public void start (Stage startStage)
	{	
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/DemoUI.fxml"));
		
		AnchorPane root = null;
		
		try {
			root = (AnchorPane)loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		SongLibController songlibcontroller = 
		         (SongLibController)loader.getController();
		
		songlibcontroller.start(startStage);
		      
		Scene scene = new Scene(root, 600, 800);
		startStage.setScene(scene);
		startStage.show(); 
		
		//TestView.run();		
	}
}