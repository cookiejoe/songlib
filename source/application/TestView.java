package application;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import model.Song;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/*
 * This class was built to test the controller and model of the program before we started
 * working on the view
 */
public class TestView {
	static ArrayList<Song> songs;
	static BufferedReader br;
	private TestView(){} // Create a private constructor to prohibit instantiation
	
	// Make it easier to type each prompt by defining the String here
	static String prompt = "Enter:\n"
			+ "\"q\" to quit\n"
			+ "\"a\" to add\n"
			+ "\"d\" to delete\n"
			+ "and \"e\" to edit\n?";
	
	public static void run()
	{
		// Initialize the BufferedReader so we can read input
		br = new BufferedReader(new InputStreamReader(System.in));
		
		// Initialize a String to hold the user's wishes
		String command;
		
		// Read and show the user the songs
		readSongs();
		printSongs();
		
		// Initialize text input
		Scanner scanner = new Scanner(System.in);
		
		// Display the prompt and get the user's wishes
		System.out.print(prompt);
		command = scanner.next().toLowerCase();
		
		//scanner.close();
		
		// Execute while the user wishes to execute
		while (!command.equals("q"))
		{
			
			// If the user wants to add a song, add it
			if (command.equals("a"))
			{
				// Declare all the used Strings
				String name = null, artist = null, album = null, year = null;
				
				// Get the info and add the song
				System.out.print("What is the song name?\n?");
				try{name = br.readLine();}catch(Exception e){System.out.println(e);}
				
				System.out.print("What is the song artist?\n?");
				try{artist = br.readLine();}catch(Exception e){System.out.println(e);}
				
				System.out.print("What is the song album?\n?");
				try{album = br.readLine();}catch(Exception e){System.out.println(e);}
				
				System.out.print("What is the song year?\n?");
				try{year = br.readLine();}catch(Exception e){System.out.println(e);}
				
				System.out.println("In run(), before addSong()\nSize of songs: " + songs.size());
				try{Song a = new Song(name, artist, album, year);}catch(Exception e){System.out.println(e);}
				System.out.println("In run(), after addSong()\nSize of songs: " + songs.size());
				System.out.print("\n\n\n");
				
			} else if (command.equals("d")) { // If the user wants to delete a song, delete it
				
				// Get the index of the black sheep song from the user and delete it
				int index = -1;
				System.out.print("Input the index of the song you wish to delete.\n?");
				try{index = Integer.parseInt(br.readLine());}catch(Exception e){System.out.println(e);}
				
				Song.deleteSong(songs.get(index));
				
				System.out.print("\n\n\n");
				
			} else if (command.equals("e")) { // If the user wants to edit a song, edit a song
				
				// Get the index of the song to change from the user
				int index = -1;
				System.out.print("Input the index of the song you wish to edit.\n?");
				try{index = Integer.parseInt(br.readLine());}catch(Exception e){System.out.println(e);}

				String name = null, artist = null, album = null, year = null;
				
				// Get the new info and edit the song
				System.out.print("What is the new song name? (press enter to stay the same)\n?");
				try{name = br.readLine();}catch(Exception e){System.out.println(e);}
				if (name.equals("")) name = songs.get(index).getName(); // If the user wants this field to be the same, keep it the same
				
				System.out.print("What is the new song artist? (press enter to stay the same)\n?");
				try{artist = br.readLine();}catch(Exception e){System.out.println(e);}
				if (artist.equals("")) artist = songs.get(index).getArtist(); // If the user wants this field to be the same, keep it the same
				
				System.out.print("What is the new song album? (press enter to stay the same)\n?");
				try{album = br.readLine();}catch(Exception e){System.out.println(e);}
				if (album.equals("")) album = songs.get(index).getAlbum(); // If the user wants this field to be the same, keep it the same
				
				System.out.print("What is the new song year? (press enter to stay the same)\n?");
				try{year = br.readLine();}catch(Exception e){System.out.println(e);}
				if (year.equals("")) year = songs.get(index).getYear(); // If the user wants this field to be the same, keep it the same
								
				Song.editSong(songs.get(index), name, artist, album, year);
				
				System.out.print("\n\n\n");
				
			}
			
			// Update and show the user the songs
			readSongs();
			printSongs();
			
			// Display the prompt and get the user's wishes
			System.out.print(prompt);
			command = scanner.next().toLowerCase();
		}
	}
	
	// Updates the songs to reflect changes
	static void readSongs()
	{
		songs = Song.getSongs();
		//System.out.println("Song.songs: "+Song.songs);
		//System.out.println("TestView.songs: "+songs);
	}
	
	/*
	 * Iterates over all the songs and prints them one-by-one to the screen
	 */
	static void printSongs()
	{
		
		// Create an Iterator for the songs
		Iterator<Song> songIter = songs.iterator();
		
		// Initialize the memory for a Song object to hold the current song
		Song song;
		
		// Set index to 0, this variable tells the user which index to find the song
		int index = 0;
		
		System.out.println("In printSongs\nSize of songs: " + songs.size());
		
		// Actually print if there are more songs. About time, right?
		while (songIter.hasNext())
		{
			// Get the next song
			song = songIter.next();
			
			// Output the song's info to the user, including the index
			System.out.format("Index: %d - %15s %15s %15s %15s\n", index,
					song.getName(), song.getArtist(), song.getAlbum(), song.getYear());			
			
			// Increment the index for the next song, if there is one
			index++;
		}
	}
}