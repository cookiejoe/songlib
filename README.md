# README #



### What is this repository for? ###

* A CS213 class. The project is to create a GUI which uses JavaFX and FXML along with Java code to store, retrieve and organize a song library

### How do I get set up? ###

* Add the json jars to your buildpath
--- They are in /Lib/
-----1) Using Eclipse, right click on the root directory in the Project Explorer (SongLib)
-----2) Navigate to Build Path
-----3) Click on Configure Build Path
-----4) Click on Add External JAR
-----5) Navigate to ~/SongLib/Lib/ and add both jars to the buildpath

* Make sure you have JavaFX installed on your machine, the developer version

### Who do I talk to? ###

* Joseph Buchoff
* Julian Dauemer