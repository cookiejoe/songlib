package model;
/*
 * SongLib Project
 * Team Byte Me;
 * Members: Joseph Buchoff, Julian Dauemer
 * 
 * Professor Seshrandi Venugopal
 * 
 * Fuel - Exercise and healthy eating
 * Hours spent - Too many
 * Walls punched - Let's not talk about it
 * 
 * Grade desired - A, obviously
 * Grade earned...? Undefined
 */

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.charset.*;

import java.util.*;

/*
 * This utility class implements some simple file i/o support to read and write to the
 * save file and implement persistence of the user's song data
 */
public class FileIO {
	// Fields
	static String filename;
	private static ArrayList<String[]> songList = new ArrayList<String[]>();	//holds arrays containing song data
	private static int index = 0;	//used to track which song array to retrieve from ArrayList
	
	private FileIO(){} // Private constructor prohibits instantiation
	
	public static void setFilename(String file)
	{
		//get full input/output JSON file pathname
		Path currentRelativePath = Paths.get("");
		String fullPathname = currentRelativePath.toAbsolutePath().toString() + "/source/model/";
		filename = fullPathname + file;
		songList = new ArrayList<String[]>();
	}
	
	//each call to nextSong() will return an array of size 4 to the callee. These arrays
	//are stored in an ArrayList
	public static String[] nextSong() {
		if (index == songList.size()) {
			return null;
		}
		return songList.get(index++);	//index value incremented
	}
	
	public static void readFile() {	//reads data from JSON file
		String[] songData = new String[4];	//holds song title, artist, album, year
		String key, value;
		Path file = Paths.get(filename);
		
		/*
		 * Check to see if file is empty 
		 */
		try {
			if (Files.size(file) == 0) {
				return;
			}
		} catch (IOException e) {
			System.out.println(e);
		}
		
		
		try (InputStream inStream = Files.newInputStream(file);
				JsonParser parser = Json.createParser(inStream)) {
				while (parser.hasNext()) {
					JsonParser.Event event = parser.next();
					if (event == Event.KEY_NAME && !(parser.getString().equalsIgnoreCase("songs"))) { //skip first key
						//System.out.print(parser.getString() + ": ");
						key = parser.getString();
						event = parser.next();
						if (event == Event.VALUE_STRING) {
							//System.out.println(parser.getString());
							value = parser.getString();
							switch (key) {
								case "title":
									songData[0] = value;
									break;
								case "artist":
									songData[1] = value;
									break;
								case "album":
									songData[2] = value;
									break;
								case "year":
									songData[3] = value;
									songList.add(songData);		//add array to ArrayList
									songData = new String[4];	//create new array for next iteration
									break;
							}
						}
					}
				}
			} 
		catch (IOException e) {
			System.out.println(e);
		}
	}
	
	public static void writeFile(ArrayList<Song> updatedSongList) {
		if (updatedSongList.isEmpty()) {	//if updatedSongList is empty, clear contents of JSON I/O file
			try {
				FileWriter fwOb = new FileWriter(filename, false); 
				PrintWriter pwOb = new PrintWriter(fwOb, false);
				pwOb.flush();
				pwOb.close();
				fwOb.close();
				return;
			} catch (IOException e) {
				System.out.println(e);
			}
		}
		
		String output = "{\n\t\"songs\": [\n";
		for (Iterator<Song> iterator = updatedSongList.iterator(); iterator.hasNext();) {
			Song s = iterator.next();
			String beginSongObj = "\t\t{ ";
			String titleString = "\"title\":" + "\"" + s.getName() + "\"" + ",";
			String artistString = "\"artist\":" + "\"" + s.getArtist() + "\"" + ",";
			String albumString = "\"album\":" + "\"" + s.getAlbum() + "\"" + ",";
			String yearString = "\"year\":" + "\"" + s.getYear() + "\"";
			String endSongObj = " }";
			if (iterator.hasNext()) {
				endSongObj += ",\n";
			}
			else {
				endSongObj += "\n\t]\n}";
			}
			output += beginSongObj + titleString + artistString + albumString + yearString + endSongObj;
		}
		//now write string to file
		Path file = Paths.get(filename);
		Charset charset = Charset.forName("US-ASCII");
		try (BufferedWriter writer = Files.newBufferedWriter(file, charset)) {
		    writer.write(output, 0, output.length());
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}
	}
}
