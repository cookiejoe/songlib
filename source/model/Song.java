package model;
/*
 * SongLib Project
 * Team Byte Me;
 * Members: Joseph Buchoff, Julian Dauemer
 * 
 * Professor Seshrandi Venugopal
 * 
 * Fuel - Exercise and healthy eating
 * Hours spent - Too many
 * Walls punched - Let's not talk about it
 * 
 * Grade desired - A, obviously
 * Grade earned...? Undefined
 */

// Import statements
import javax.json.*;

import controller.SongLibController;

import java.util.ArrayList;
import java.util.Collections;

/*
 * This class holds the Song object.
 * A song has an Artist and a Name.
 * We include a plain text output in Song.getText() to easily save songs to file as JSON text
 * We include a constructor which takes in JSON to easily load from the text file upon open
 * We override compareTo(Song other) to suit our needs (no songs with the same name AND same artist)
 */
public class Song implements Comparable<Song>{
	// Static fields
	private static ArrayList<Song> songs = new ArrayList<Song>();
	
	// Instance fields
	private String name;
	private String artist;
	private String album;
	private String year;
	
	/* CONSTRUCTOR */
	
	// Constructor for when called using an add() event from the Add button in the view
	public Song(String name, String artist, String album, String year) throws SameSongException
	{	
		// Check for duplicates, throw exception if there is one
		if (Song.searchSong(name, artist, 0))
		{
			throw new SameSongException();
		}
		
		this.name = name;
		this.artist = artist;
		this.album = album;
		this.year = year;
		
		// Add this new song to the static masterlist of Songs
		Song.addSong(this);
	}
	
	/* UTILITY METHODS */
	
	// Static methods
	/*
	 * Static method to add a song to the ArrayList containing all the songs
	 * Package-level privacy
	 * It sorts the ArrayList by name after adding
	 */
	public static void addSong(Song addition) throws SameSongException
	{
		if (searchSong(addition.getName(), addition.getArtist(), 0))
		{
			throw new SameSongException();
		}

		getSongs().add(addition);

		Collections.sort(getSongs());
;
	}
	
	/*
	 * Static method to delete a song from the ArrayList containing all the songs
	 * Package-level privacy
	 * It sorts the ArrayList by name after adding
	 */
	public static void deleteSong(Song deletion)
	{
		getSongs().remove(deletion);
		Collections.sort(getSongs());
	}
	
	/* TODO
	 * Static method to edit a song from the ArrayList containing all the songs
	 * Package-level privacy
	 * It sorts the ArrayList by name after editing
	 */
	public static void editSong(Song toEdit, String name, String artist, String album, String year) throws SameSongException
	{
		// If the user didn't edit anything, don't change anything and exit
		if (toEdit.getName().equals(name) && toEdit.getArtist().equals(artist) && toEdit.getAlbum().equals(album) && toEdit.getYear().equals(year))
			return;
		
		Song newSong = null;
		
		// Create a new Song with the new info
		newSong = new Song(name, artist, album, year);
		
		// Remove the old song
		getSongs().remove(toEdit);
		
		// Sort the song list
		Collections.sort(getSongs());
	}
	
	
	/* GETTERS */
	
	// STATIC Getter for Song.songs
	public static ArrayList<Song> getSongs() {
		return songs;
	}
	
	// Getter for String name
	public String getName()
	{
		return name;
	}
	
	// Getter for String artist
	public String getArtist()
	{
		return artist;
	}
	
	// Getter for String artist
	public String getAlbum()
	{
		return album;
	}
	
	// Getter for String artist
	public String getYear()
	{
		return year;
	}
	
	// Getter for displayable list of songs
	public static ArrayList<String> getDisplayableList(int selectedIndex)
	{
		// Declare and initialize the ArrayList we're going to return
		ArrayList<String> displayableList = new ArrayList<String>();
		
		// Iterate through all songs in Song.Songs and add their names to the ArrayList
		// unless it's selected. In that case add all its info
		for (Song curr : songs)
		{
			// If the current song is selected, add all its info to the ArrayList. Otherwise just add its name.
			if (songs.indexOf(curr) == selectedIndex)
				displayableList.add(curr.toString());
			else
				displayableList.add(curr.getName());
		}
		
		return displayableList;
	}
	
	/* SETTERS */
	
	// Static setter for Song.songs
	public static void setSongs(ArrayList<Song> songs) {
		Song.songs = songs;
	}
	
	/* PARTIAL COMPARE METHODS */
	
	/*
	 *  Public compare-to for the name field to ease sorting. Returns 0 if they are the same,
	 *  1 if this Song's name comes after alphabetically, and -1 if this Song's name comes before
	 */
	public int compareName(Song other)
	{
		return compareTo(other); // Just use the functionality I baked into the compareTo() method for brevity
	}
	
	/*
	 * Public compare-to method for the artist field to ease sorting. Returns 0 if they are the same,
	 * 1 if this Song's artist comes after alphabetically, and -1 if this Song's artist comes before
	 */
	public int compareArtist(Song other)
	{
		return this.getArtist().toLowerCase().compareTo(other.getArtist().toLowerCase()); // Compare the strings and see
	}

	/* OVERRIDE METHODS */
	
	// Override equals method to make it functional
	@Override
	public boolean equals(Object other)
	{
		return (other instanceof Song && ((Song)other).getArtist().toLowerCase().equals(artist.toLowerCase())
				&& ((Song)other).getName().toLowerCase().equals(name.toLowerCase()))
			? true : false;
	}
	
	// Public compare-to for other songs, to sort and weed out duplicates. Also includes compare name functionality for sorting
	@Override
	public int compareTo(Song other)
	{
		if (other instanceof Song && other.getName().toLowerCase().equals(this.name.toLowerCase())
				&& other.getArtist().toLowerCase().equals(this.artist.toLowerCase()))
		{
			return 0;
		}
		
		return this.getName().toLowerCase().compareTo(other.getName().toLowerCase());
	}
	
	@Override
	public String toString()
	{
		String string = String.format("%20s %20s %20s %20s", name, artist, album, year);
		return string;
	}
	
	// Recursive method because we can. Returns true if we already have a song with that name and artist
	private static boolean searchSong(String name, String artist, int index)
	{
		// End cases
		// If we're at the end of the road and we can't go on
		if (index == getSongs().size())
		{
			return false;
		}
		
		// If we have found a match
		if (getSongs().get(index).getName().toLowerCase().equals(name.toLowerCase()) && getSongs().get(index).getArtist().toLowerCase().equals(artist.toLowerCase()))
		{
			return true;
		}
		
		return searchSong(name, artist, ++index);
	}
}